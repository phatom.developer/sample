import 'package:flutter/material.dart';

///
class UnfocusDetector extends StatelessWidget {
  ///
  const UnfocusDetector({
    Key? key,
    this.child,
  }) : super(key: key);

  final Widget? child;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: child,
      );
}
