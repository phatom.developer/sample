import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:alecia_app/src/app_theme/colors.dart';

/// Supported back button with some widget
class TextBackButton extends StatelessWidget {
  ///
  const TextBackButton({
    this.backIcon = const Icon(Icons.arrow_back_ios_new_rounded),
    this.text,
    this.onPressed,
    Key? key,
  }) : super(key: key);

  ///
  final Icon backIcon;

  ///
  final Widget? text;

  ///
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) => TextButton(
        onPressed: onPressed ?? Navigator.of(context).maybePop,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            backIcon,
            Expanded(
              child: DefaultTextStyle(
                  style: const TextStyle(
                    color: AlesiaColors.lightBlue,
                    fontSize: 17,
                    fontWeight: FontWeight.w400,
                  ),
                  child: text ??
                      Text(AppLocalizations.of(context)?.back ?? 'back')),
            )
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(ObjectFlagProperty<VoidCallback?>.has('onPressed', onPressed));
  }
}
