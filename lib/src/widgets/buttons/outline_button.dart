import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Fill button
class AlesiaOutlineButton extends StatelessWidget {
  /// Default constructor
  const AlesiaOutlineButton({
    required this.child,
    this.onPressed,
    Key? key,
    this.color = Colors.black,
  }) : super(key: key);

  /// Void callback on tap on the button
  final VoidCallback? onPressed;

  /// Child of the button
  final Widget child;

  ///
  final Color color;

  @override
  Widget build(BuildContext context) => Center(
        child: OutlinedButton(
            onPressed: onPressed,
            style: OutlinedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(14),
              ),
              side: BorderSide(width: 3, color: color),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 18),
              child: child,
            )),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(ObjectFlagProperty<VoidCallback>.has('onPressed', onPressed));
  }
}
