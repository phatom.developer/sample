import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Fill button
class FillButton extends StatelessWidget {
  /// Default constructor
  const FillButton({
    required this.child,
    this.onPressed,
    Key? key,
  }) : super(key: key);

  /// Void callback on tap on the button
  final VoidCallback? onPressed;

  /// Child of the button
  final Widget child;

  @override
  Widget build(BuildContext context) => Center(
        child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
            primary: Colors.white,
            textStyle: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14),
            ),
            backgroundColor: const Color(0xff0099ff),
            // shape: const StadiumBorder()
            minimumSize: Size(MediaQuery.of(context).size.width - 70, 0),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 18),
            child: child,
          ),
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(ObjectFlagProperty<VoidCallback>.has('onPressed', onPressed));
  }
}
