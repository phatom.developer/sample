import 'dart:ui';

/// App theme colors
abstract class AlesiaColors {
  /// alecia-light-blue
  static const Color lightBlue = Color(0xff0099ff);
}
