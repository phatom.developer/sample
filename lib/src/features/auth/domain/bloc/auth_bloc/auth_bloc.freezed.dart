// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

  AuthorizedState authorized(User user) {
    return AuthorizedState(
      user,
    );
  }

  UnauthorizedState unauthorized() {
    return const UnauthorizedState();
  }
}

/// @nodoc
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) authorized,
    required TResult Function() unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? authorized,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedState value) authorized,
    required TResult Function(UnauthorizedState value) unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedState value)? authorized,
    TResult Function(UnauthorizedState value)? unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;
}

/// @nodoc
abstract class $AuthorizedStateCopyWith<$Res> {
  factory $AuthorizedStateCopyWith(
          AuthorizedState value, $Res Function(AuthorizedState) then) =
      _$AuthorizedStateCopyWithImpl<$Res>;
  $Res call({User user});
}

/// @nodoc
class _$AuthorizedStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements $AuthorizedStateCopyWith<$Res> {
  _$AuthorizedStateCopyWithImpl(
      AuthorizedState _value, $Res Function(AuthorizedState) _then)
      : super(_value, (v) => _then(v as AuthorizedState));

  @override
  AuthorizedState get _value => super._value as AuthorizedState;

  @override
  $Res call({
    Object? user = freezed,
  }) {
    return _then(AuthorizedState(
      user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }
}

/// @nodoc

class _$AuthorizedState implements AuthorizedState {
  const _$AuthorizedState(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'AuthState.authorized(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AuthorizedState &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @JsonKey(ignore: true)
  @override
  $AuthorizedStateCopyWith<AuthorizedState> get copyWith =>
      _$AuthorizedStateCopyWithImpl<AuthorizedState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) authorized,
    required TResult Function() unauthorized,
  }) {
    return authorized(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? authorized,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedState value) authorized,
    required TResult Function(UnauthorizedState value) unauthorized,
  }) {
    return authorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedState value)? authorized,
    TResult Function(UnauthorizedState value)? unauthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(this);
    }
    return orElse();
  }
}

abstract class AuthorizedState implements AuthState {
  const factory AuthorizedState(User user) = _$AuthorizedState;

  User get user => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthorizedStateCopyWith<AuthorizedState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnauthorizedStateCopyWith<$Res> {
  factory $UnauthorizedStateCopyWith(
          UnauthorizedState value, $Res Function(UnauthorizedState) then) =
      _$UnauthorizedStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnauthorizedStateCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements $UnauthorizedStateCopyWith<$Res> {
  _$UnauthorizedStateCopyWithImpl(
      UnauthorizedState _value, $Res Function(UnauthorizedState) _then)
      : super(_value, (v) => _then(v as UnauthorizedState));

  @override
  UnauthorizedState get _value => super._value as UnauthorizedState;
}

/// @nodoc

class _$UnauthorizedState implements UnauthorizedState {
  const _$UnauthorizedState();

  @override
  String toString() {
    return 'AuthState.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnauthorizedState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) authorized,
    required TResult Function() unauthorized,
  }) {
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? authorized,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedState value) authorized,
    required TResult Function(UnauthorizedState value) unauthorized,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedState value)? authorized,
    TResult Function(UnauthorizedState value)? unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class UnauthorizedState implements AuthState {
  const factory UnauthorizedState() = _$UnauthorizedState;
}

/// @nodoc
class _$AuthEventTearOff {
  const _$AuthEventTearOff();

  LogoutEvent logout() {
    return const LogoutEvent();
  }

  EmailAndPasswordLoginEvent emailAndPasswordLogin(
      {required String email, required String password}) {
    return EmailAndPasswordLoginEvent(
      email: email,
      password: password,
    );
  }
}

/// @nodoc
const $AuthEvent = _$AuthEventTearOff();

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function(String email, String password)
        emailAndPasswordLogin,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function(String email, String password)? emailAndPasswordLogin,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LogoutEvent value) logout,
    required TResult Function(EmailAndPasswordLoginEvent value)
        emailAndPasswordLogin,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LogoutEvent value)? logout,
    TResult Function(EmailAndPasswordLoginEvent value)? emailAndPasswordLogin,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res> implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  final AuthEvent _value;
  // ignore: unused_field
  final $Res Function(AuthEvent) _then;
}

/// @nodoc
abstract class $LogoutEventCopyWith<$Res> {
  factory $LogoutEventCopyWith(
          LogoutEvent value, $Res Function(LogoutEvent) then) =
      _$LogoutEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$LogoutEventCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements $LogoutEventCopyWith<$Res> {
  _$LogoutEventCopyWithImpl(
      LogoutEvent _value, $Res Function(LogoutEvent) _then)
      : super(_value, (v) => _then(v as LogoutEvent));

  @override
  LogoutEvent get _value => super._value as LogoutEvent;
}

/// @nodoc

class _$LogoutEvent implements LogoutEvent {
  const _$LogoutEvent();

  @override
  String toString() {
    return 'AuthEvent.logout()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LogoutEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function(String email, String password)
        emailAndPasswordLogin,
  }) {
    return logout();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function(String email, String password)? emailAndPasswordLogin,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LogoutEvent value) logout,
    required TResult Function(EmailAndPasswordLoginEvent value)
        emailAndPasswordLogin,
  }) {
    return logout(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LogoutEvent value)? logout,
    TResult Function(EmailAndPasswordLoginEvent value)? emailAndPasswordLogin,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout(this);
    }
    return orElse();
  }
}

abstract class LogoutEvent implements AuthEvent {
  const factory LogoutEvent() = _$LogoutEvent;
}

/// @nodoc
abstract class $EmailAndPasswordLoginEventCopyWith<$Res> {
  factory $EmailAndPasswordLoginEventCopyWith(EmailAndPasswordLoginEvent value,
          $Res Function(EmailAndPasswordLoginEvent) then) =
      _$EmailAndPasswordLoginEventCopyWithImpl<$Res>;
  $Res call({String email, String password});
}

/// @nodoc
class _$EmailAndPasswordLoginEventCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements $EmailAndPasswordLoginEventCopyWith<$Res> {
  _$EmailAndPasswordLoginEventCopyWithImpl(EmailAndPasswordLoginEvent _value,
      $Res Function(EmailAndPasswordLoginEvent) _then)
      : super(_value, (v) => _then(v as EmailAndPasswordLoginEvent));

  @override
  EmailAndPasswordLoginEvent get _value =>
      super._value as EmailAndPasswordLoginEvent;

  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
  }) {
    return _then(EmailAndPasswordLoginEvent(
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$EmailAndPasswordLoginEvent implements EmailAndPasswordLoginEvent {
  const _$EmailAndPasswordLoginEvent(
      {required this.email, required this.password});

  @override
  final String email;
  @override
  final String password;

  @override
  String toString() {
    return 'AuthEvent.emailAndPasswordLogin(email: $email, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EmailAndPasswordLoginEvent &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password);

  @JsonKey(ignore: true)
  @override
  $EmailAndPasswordLoginEventCopyWith<EmailAndPasswordLoginEvent>
      get copyWith =>
          _$EmailAndPasswordLoginEventCopyWithImpl<EmailAndPasswordLoginEvent>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function(String email, String password)
        emailAndPasswordLogin,
  }) {
    return emailAndPasswordLogin(email, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function(String email, String password)? emailAndPasswordLogin,
    required TResult orElse(),
  }) {
    if (emailAndPasswordLogin != null) {
      return emailAndPasswordLogin(email, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LogoutEvent value) logout,
    required TResult Function(EmailAndPasswordLoginEvent value)
        emailAndPasswordLogin,
  }) {
    return emailAndPasswordLogin(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LogoutEvent value)? logout,
    TResult Function(EmailAndPasswordLoginEvent value)? emailAndPasswordLogin,
    required TResult orElse(),
  }) {
    if (emailAndPasswordLogin != null) {
      return emailAndPasswordLogin(this);
    }
    return orElse();
  }
}

abstract class EmailAndPasswordLoginEvent implements AuthEvent {
  const factory EmailAndPasswordLoginEvent(
      {required String email,
      required String password}) = _$EmailAndPasswordLoginEvent;

  String get email => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EmailAndPasswordLoginEventCopyWith<EmailAndPasswordLoginEvent>
      get copyWith => throw _privateConstructorUsedError;
}
