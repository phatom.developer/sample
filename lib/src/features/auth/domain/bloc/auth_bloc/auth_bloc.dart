import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pedantic/pedantic.dart';

part 'auth_state.dart';
part 'auth_event.dart';
part 'auth_bloc.freezed.dart';

/// Auth bloc of the app
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  /// Default constructor
  AuthBloc()
      : super(FirebaseAuth.instance.currentUser != null
            ? AuthorizedState(FirebaseAuth.instance.currentUser!)
            : const UnauthorizedState()) {
    _subscribeToChangeFirebaseAuthState();
  }

  late StreamSubscription<User?> _subscribtion;

  void _subscribeToChangeFirebaseAuthState() {
    _subscribtion =
        FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        emit(AuthState.authorized(user));
      } else {
        emit(const AuthState.unauthorized());
      }
    });
  }

  Stream<AuthState> _onEmailAndPasswordLogin(
    String email,
    String password,
  ) async* {}

  /// Logout from the app
  Stream<AuthState> _onLogout() async* {
    unawaited(FirebaseAuth.instance.signOut());
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    yield* event.when(
      logout: _onLogout,
      emailAndPasswordLogin: _onEmailAndPasswordLogin,
    );
  }

  @override
  Future<void> close() => Future.wait([
        super.close(),
        _subscribtion.cancel(),
      ]);
}
