part of 'auth_bloc.dart';

@freezed
abstract class AuthEvent with _$AuthEvent {
  const factory AuthEvent.logout() = LogoutEvent;
  const factory AuthEvent.emailAndPasswordLogin({
    required String email,
    required String password,
  }) = EmailAndPasswordLoginEvent;
}
