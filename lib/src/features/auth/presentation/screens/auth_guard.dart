import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'package:alecia_app/src/features/auth/domain/bloc/auth_bloc/auth_bloc.dart';

/// Auth guard scope, protects the user from entering the application
/// without the necessary authorization data
@immutable
class AuthGuard extends StatefulWidget {
  ///
  const AuthGuard({
    required this.child,
    required this.authScreen,
    Key? key,
  }) : super(key: key);

  /// Child widget, it's main app with [User] in the [BuildContext] context,
  /// that be added in this
  /// state
  final Widget child;

  /// Auth screen or navigation, must be show if user don't have authoirisation
  /// data
  final Widget authScreen;

  /// Fing [_AuthScope] state in the [BuildContext] context
  static _AuthGuardState? of(BuildContext context) =>
      _AuthScope.of(context)?.state;

  @override
  State<AuthGuard> createState() => _AuthGuardState();
}

class _AuthGuardState extends State<AuthGuard> {
  late AuthBloc? _bloc;

  void logout() {
    _bloc?.add(const LogoutEvent());
  }

  void loginWithEmailAndPassword({
    required String email,
    required String password,
  }) {
    _bloc?.add(EmailAndPasswordLoginEvent(email: email, password: password));
  }

  @override
  void initState() {
    super.initState();
    _bloc = AuthBloc();
  }

  @override
  void dispose() {
    super.dispose();
    _bloc?.close();
  }

  @override
  Widget build(BuildContext context) => _AuthScope(
        state: this,
        child: BlocBuilder<AuthBloc, AuthState>(
          bloc: _bloc,
          builder: (BuildContext context, AuthState state) => state.when(
            authorized: (User user) => Provider<User>.value(
              value: user,
              child: widget.child,
            ),
            unauthorized: () => widget.authScreen,
          ),
        ),
      );
}

@immutable
class _AuthScope extends InheritedWidget {
  const _AuthScope({
    required Widget child,
    required this.state,
    Key? key,
  }) : super(key: key, child: child);

  final _AuthGuardState state;

  /// Find _AuthScope in BuildContext
  static _AuthScope? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<_AuthScope>();

  @override
  bool updateShouldNotify(_AuthScope oldWidget) =>
      !identical(state, oldWidget.state) || state != oldWidget.state;
}
