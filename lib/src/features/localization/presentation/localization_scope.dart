import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:alecia_app/app/l10n/l10n.dart';

/// App localization
class AppLocalization extends StatefulWidget {
  /// Default construcor
  const AppLocalization({
    required this.child,
    Key? key,
  }) : super(key: key);

  /// Child widget, he and his children will have localization in the context
  final Widget child;

  static Locale get defaultLocale => const Locale('en');

  /// Find the AppLocalization
  static _AppLocalizationState? of(BuildContext context) =>
      _AppLocalizationScope.of(context)?.state;

  @override
  _AppLocalizationState createState() => _AppLocalizationState();
}

class _AppLocalizationState extends State<AppLocalization> {
  late Locale currentLocale;

  void updateLocale({required Locale to}) {
    if (to == currentLocale || !L10n.supportedLocales.contains(to)) {
      return;
    }
    setState(() {
      currentLocale = to;
    });
  }

  @override
  void initState() {
    super.initState();
    currentLocale = L10n.defaultLocale;
  }

  @override
  Widget build(BuildContext context) => _AppLocalizationScope(
        state: this,
        child: Localizations(
          locale: currentLocale,
          delegates: AppLocalizations.localizationsDelegates,
          child: widget.child,
        ),
      );
}

///
@immutable
class _AppLocalizationScope extends InheritedWidget {
  const _AppLocalizationScope({
    required Widget child,
    required this.state,
    Key? key,
  }) : super(key: key, child: child);

  ///
  final _AppLocalizationState state;

  /// Find _AuthScope in BuildContext
  static _AppLocalizationScope? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<_AppLocalizationScope>();

  @override
  bool updateShouldNotify(_AppLocalizationScope oldWidget) =>
      !identical(state, oldWidget.state) ||
      state.currentLocale != oldWidget.state.currentLocale;
}
