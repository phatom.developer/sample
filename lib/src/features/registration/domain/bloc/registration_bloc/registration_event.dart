part of 'registration_bloc.dart';

///
@freezed
abstract class RegistrationEvent with _$RegistrationEvent {
  ///
  factory RegistrationEvent.registration(RegistrationUserModel model) =
      Registration;
}
