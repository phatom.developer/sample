part of 'registration_bloc.dart';

@freezed
abstract class RegistrationState with _$RegistrationState {
  const factory RegistrationState.initial() = Initial;
  const factory RegistrationState.loading() = Loading;
  const factory RegistrationState.registerd() = Registered;
  const factory RegistrationState.failure() = Failure;

}