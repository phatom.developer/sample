import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'registration_state.dart';
part 'registration_event.dart';
part 'registration_bloc.freezed.dart';

/// Registration bloc
class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {

  ///
  RegistrationBloc() : super(const Initial());

  Stream<RegistrationState> _registration(RegistrationUserModel model) async* {

  }

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    yield* event.when(registration: _registration);
  }
}
