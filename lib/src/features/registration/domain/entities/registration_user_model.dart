import 'package:alecia_app/src/core/entities/country_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:alecia_app/src/core/entities/characteristics/characteristic.dart';
import 'package:alecia_app/src/core/entities/country.dart';
import 'package:alecia_app/src/core/entities/language.dart';

/// Supported model for step by step filling user info
@immutable
class RegistrationUserModel extends Equatable {
  /// Default constructor
  const RegistrationUserModel({
    this.gender,
    this.language,
    this.name,
    this.birtchDay,
    this.residence,
    this.city,
    this.languageSkills,
    this.education,
    this.workArea,
    this.height,
    this.eyeColor,
    this.hairColor,
    this.smoking,
    this.characters,
    this.martialStatus,
    this.children,
    this.hobbies,
    this.sports,
    this.visitedCountries,
    this.phone,
    this.email,
    this.password,
  });

  ///
  final Gender? gender;

  /// Only for users, who have [Gender.man]
  final Language? language;

  ///
  final String? name;

  ///
  final DateTime? birtchDay;

  ///
  final CountryModel? residence;

  ///
  final String? city;

  ///
  final LanguageSkills? languageSkills;

  ///
  final Education? education;

  ///
  final WorkArea? workArea;

  /// In CM, for convert use num extension
  final int? height;

  ///
  final EyeColor? eyeColor;

  ///
  final HairColor? hairColor;

  ///
  final bool? smoking;

  ///
  final List<Character>? characters;

  ///
  final MartialStatus? martialStatus;

  ///
  final Children? children;

  ///
  final List<Hobby>? hobbies;

  ///
  final List<Sport>? sports;

  ///
  final List<Country>? visitedCountries;

  ///
  final String? phone;

  ///
  final String? email;

  ///
  final String? password;

  @override
  String toString() => '''
  RegistrationUserModel(gender: $gender, language: $language, name: $name, birtchDay: $birtchDay, residence: $residence, sity: $city, languageSkills: $languageSkills, education: $education, workArea: $workArea, height: $height, eyeColor: $eyeColor, hairColor: $hairColor, smoking: $smoking, characters: $characters, martialStatus: $martialStatus, children: $children, hobbies: $hobbies, sports: $sports, visitedCountries: $visitedCountries, phone: $phone, email: $email, password: $password)
  ''';

  RegistrationUserModel copyWith({
    Gender? gender,
    Language? language,
    String? name,
    DateTime? birtchDay,
    CountryModel? residence,
    String? city,
    LanguageSkills? languageSkills,
    Education? education,
    WorkArea? workArea,
    int? height,
    EyeColor? eyeColor,
    HairColor? hairColor,
    bool? smoking,
    List<Character>? characters,
    MartialStatus? martialStatus,
    Children? children,
    List<Hobby>? hobbies,
    List<Sport>? sports,
    List<Country>? visitedCountries,
    String? phone,
    String? email,
    String? password,
  }) =>
      RegistrationUserModel(
        gender: gender ?? this.gender,
        language: language ?? this.language,
        name: name ?? this.name,
        birtchDay: birtchDay ?? this.birtchDay,
        residence: residence ?? this.residence,
        city: city ?? this.city,
        languageSkills: languageSkills ?? this.languageSkills,
        education: education ?? this.education,
        workArea: workArea ?? this.workArea,
        height: height ?? this.height,
        eyeColor: eyeColor ?? this.eyeColor,
        hairColor: hairColor ?? this.hairColor,
        smoking: smoking ?? this.smoking,
        characters: characters ?? this.characters,
        martialStatus: martialStatus ?? this.martialStatus,
        children: children ?? this.children,
        hobbies: hobbies ?? this.hobbies,
        sports: sports ?? this.sports,
        visitedCountries: visitedCountries ?? this.visitedCountries,
        phone: phone ?? this.phone,
        email: email ?? this.email,
        password: password ?? this.password,
      );

  @override
  List<Object?> get props => [
        gender,
        language,
        name,
        birtchDay,
        residence,
        city,
        languageSkills,
        education,
        workArea,
        height,
        eyeColor,
        hairColor,
        smoking,
        characters,
        martialStatus,
        children,
        hobbies,
        sports,
        visitedCountries,
        phone,
        email,
        password,
      ];
}
