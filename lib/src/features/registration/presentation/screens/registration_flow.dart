import 'package:alecia_app/src/core/entities/country.dart';
import 'package:alecia_app/src/core/entities/country_model.dart';
import 'package:alecia_app/src/core/entities/language.dart';
import 'package:alecia_app/src/features/localization/presentation/localization_scope.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/birtchday_step.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/name_step.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/residence_city_step.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/residence_step.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:alecia_app/src/core/entities/characteristics/gender.dart';
import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/gender_step.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_steps/language_step.dart';

/// Regiastration user page generator
List<Page<void>> onGenerateProfilePages(
  RegistrationUserModel userModel,
  List<Page<void>> pages,
) =>
    <Page<void>>[
      const MaterialPage<void>(
        child: GenderStep(),
        name: '/registration-user',
      ),
      if (userModel.gender != null &&
          userModel.gender!.interfaceLanguages.length > 1)
        MaterialPage<void>(
          child: LanguageStep(
            supportedLanguages: userModel.gender!.interfaceLanguages,
          ),
        ),
      if (userModel.language != null)
        const MaterialPage<void>(child: NameStep()),
      if (userModel.name != null)
        const CupertinoPage<void>(child: BirtchdayStep()),
      if (userModel.birtchDay != null)
        MaterialPage<void>(
          child: ResidenceStep(
              supportedCountries: Countries.residenceFor(userModel.gender!)
                  .map((Country country) => CountryModel(country))
                  .toList()),
        ),
      if (userModel.residence != null)
        const MaterialPage<void>(
          child: ResidenceStepStep(),
        ),
      if (userModel.city != null)
        MaterialPage<void>(
          child: Container(
            color: Colors.blue,
          ),
        ),
    ];

/// Registration: gender flow,
class RegistrationUserFlow extends StatefulWidget {
  ///
  const RegistrationUserFlow({
    Key? key,
  }) : super(key: key);

  @override
  _RegistrationUserFlowState createState() => _RegistrationUserFlowState();
}

class _RegistrationUserFlowState extends State<RegistrationUserFlow> {
  late final FlowController<RegistrationUserModel> controller;

  @override
  void initState() {
    super.initState();
    print('debug build');
    controller =
        FlowController<RegistrationUserModel>(const RegistrationUserModel())
          ..addListener(() {
            final RegistrationUserModel model = controller.state;
            debugPrint(model.toString());
            if (model.gender == Gender.woman && model.language == null) {
              controller.update((RegistrationUserModel model) =>
                  model.copyWith(language: Language.russian));
              AppLocalization.of(context)
                  ?.updateLocale(to: Language.english.locale);
            }
            if (model.language != null && model.name == null) {
              final Locale locale = model.language!.locale;
              AppLocalization.of(context)?.updateLocale(to: locale);
            }
          });
  }

  @override
  Widget build(BuildContext context) => FlowBuilder<RegistrationUserModel>(
        controller: controller,
        onGeneratePages: onGenerateProfilePages,
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<FlowController<RegistrationUserModel>>(
        'controller', controller));
  }
}

/// RegistrationUserController
class RegistrationUserController extends FlowController<RegistrationUserModel> {
  /// Default constructor
  RegistrationUserController(RegistrationUserModel state) : super(state);
}

/// Language supported extension
extension LanguageLocale on Language {
  /// Locale for current language
  Locale get locale => Locale(languageCode);
}

///
class RegistrationFlowNavigationObserver extends NavigatorObserver {
  ///
  RegistrationFlowNavigationObserver(this.controller);

  final FlowController<RegistrationUserModel> controller;

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
  }
}
