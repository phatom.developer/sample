import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:alecia_app/src/features/registration/presentation/widgets/template_registration_step.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:intl/intl.dart';

/// Name flow ster for select name of the user
class BirtchdayStep extends StatefulWidget {
  /// Default contruector
  const BirtchdayStep({Key? key}) : super(key: key);

  @override
  _BirtchdayStepState createState() => _BirtchdayStepState();
}

class _BirtchdayStepState extends State<BirtchdayStep> {
  DateTime? _dateTime;

  void _complete() {
    final DateTime? birtchday = _dateTime;
    if (birtchday == null) {
      return;
    }
    context.flow<RegistrationUserModel>().update(
        (RegistrationUserModel userModel) =>
            userModel.copyWith(birtchDay: birtchday));
  }

  @override
  Widget build(BuildContext context) => TemplateRegistrationStep(
        // TODO: need redesign of every date picker screen
        title: Text(AppLocalizations.of(context)?.birthday ?? 'yourBirtchDay'),
        onNextPressed: _complete,
        child: Center(
          child: TextButton(
            onPressed: () async {
              _dateTime = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1950),
                lastDate: DateTime.now(),
              );
              setState(() {});
            },
            child: Text(_dateTime == null
                ? 'tap for select date'
                : DateFormat('yyyy.MM.dd').format(_dateTime!)),
          ),
        ),
      );
}
