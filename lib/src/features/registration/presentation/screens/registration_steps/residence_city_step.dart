import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:alecia_app/src/features/registration/presentation/widgets/template_registration_step.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flow_builder/flow_builder.dart';

/// Name flow ster for select name of the user
class ResidenceStepStep extends StatefulWidget {
  /// Default contruector
  const ResidenceStepStep({Key? key}) : super(key: key);

  @override
  _ResidenceStepStepState createState() => _ResidenceStepStepState();
}

class _ResidenceStepStepState extends State<ResidenceStepStep> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  void _complete() {
    final String text = _controller.text;
    if (text.isEmpty) {
      return;
    }
    context.flow<RegistrationUserModel>().update(
        (RegistrationUserModel userModel) => userModel.copyWith(city: text));
  }

  @override
  Widget build(BuildContext context) {
    final InputBorder border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(14),
      borderSide: const BorderSide(width: 2),
    );
    return TemplateRegistrationStep(
      title: Text(AppLocalizations.of(context)?.yourCityOfResidence ??
          'yourCityOfResidence'),
      onNextPressed: _complete,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  AppLocalizations.of(context)?.yourCityOfResidence ??
                      'yourCityOfResidence',
                  style: Theme.of(context).textTheme.headline1,
                ),
                const SizedBox(height: 36),
                TextField(
                  controller: _controller,
                  textAlign: TextAlign.center,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                    focusedBorder: border,
                    enabledBorder: border,
                    hintText: '',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
