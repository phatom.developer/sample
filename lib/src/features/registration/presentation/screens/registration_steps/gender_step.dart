import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:alecia_app/src/app_theme/colors.dart';
import 'package:alecia_app/src/core/entities/characteristics/gender.dart';
import 'package:alecia_app/src/widgets/buttons/fill_button.dart';
import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';

/// Gender flow of registration
class GenderStep extends StatefulWidget {
  /// Default constuctor
  const GenderStep({Key? key}) : super(key: key);

  @override
  _GenderStepState createState() => _GenderStepState();
}

class _GenderStepState extends State<GenderStep> {
  void _continue(Gender gender) {
    context.flow<RegistrationUserModel>().update(
        (RegistrationUserModel userModel) => userModel.copyWith(
            gender: gender,
            language: gender.interfaceLanguages.length == 1
                ? gender.interfaceLanguages.first
                : null));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    'I am',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 64,
                    ),
                  ),
                  const SizedBox(height: 36),
                  FillButton(
                    onPressed: () {
                      _continue(Gender.man);
                    },
                    child: const Text('Man'),
                  ),
                  const SizedBox(height: 18),
                  FillButton(
                    onPressed: () {
                      _continue(Gender.woman);
                    },
                    child: const Text('Woman'),
                  )
                ],
              ),
              Expanded(
                flex: 3,
                child: Container(),
              ),
              RichText(
                text: TextSpan(
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.black),
                  children: <TextSpan>[
                    const TextSpan(text: 'Already have account?'),
                    TextSpan(
                        text: ' Log in',
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: AlesiaColors.lightBlue,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            debugPrint('Action: navigate to login');
                          }),
                  ],
                ),
              ),
              Expanded(
                child: Container(),
              ),
            ],
          ),
        ),
      );
}
