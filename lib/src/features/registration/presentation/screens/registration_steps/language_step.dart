import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:alecia_app/src/core/entities/language.dart';
import 'package:alecia_app/src/core/extensions/string.dart';
import 'package:alecia_app/src/widgets/buttons/fill_button.dart';
import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:alecia_app/src/features/registration/presentation/widgets/template_registration_step.dart';

/// LanguageFlowStep
class LanguageStep extends StatefulWidget {
  /// Default constructor
  const LanguageStep({
    required this.supportedLanguages,
    Key? key,
  }) : super(key: key);

  /// User gender, define a possible list of languages
  final List<Language> supportedLanguages;

  @override
  _LanguageStepState createState() => _LanguageStepState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        IterableProperty<Language>('supportedLanguages', supportedLanguages));
  }
}

class _LanguageStepState extends State<LanguageStep> {
  void _continue(Language language) {
    context.flow<RegistrationUserModel>().update(
        (RegistrationUserModel userModel) =>
            userModel.copyWith(language: language));
  }

  @override
  Widget build(BuildContext context) => TemplateRegistrationStep(
        onBackPressed: () {
          Navigator.of(context).maybePop();
        },
        title: Text(AppLocalizations.of(context)?.language ?? 'or'),
        child: Center(
          child: ListView.separated(
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return Center(
                  child: Text(
                    AppLocalizations.of(context)?.chooseYourLanguage ?? '',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                );
              } else {
                final Language language = widget.supportedLanguages[index - 1];
                final String title = language.rawValue.capitalize;
                return FillButton(
                  onPressed: () => _continue(language),
                  child: Text(title),
                );
              }
            },
            separatorBuilder: (BuildContext context, int index) => SizedBox(
              height: index == 0 ? 36 : 18,
            ),
            itemCount: widget.supportedLanguages.length + 1,
          ),
        ),
      );
}
