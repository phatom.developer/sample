import 'package:alecia_app/src/app_theme/colors.dart';
import 'package:alecia_app/src/core/entities/characteristics/gender.dart';
import 'package:alecia_app/src/core/entities/country.dart';
import 'package:alecia_app/src/core/entities/country_model.dart';
import 'package:alecia_app/src/features/localization/presentation/localization_scope.dart';
import 'package:alecia_app/src/features/registration/domain/entities/registration_user_model.dart';
import 'package:alecia_app/src/features/registration/presentation/widgets/template_registration_step.dart';
import 'package:alecia_app/src/widgets/buttons/outline_button.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:twemoji/twemoji.dart';

/// Name flow ster for select name of the user
class ResidenceStep extends StatefulWidget {
  /// Default contruector
  const ResidenceStep({
    this.supportedCountries = const <CountryModel>[],
    Key? key,
  }) : super(key: key);

  ///
  final List<CountryModel> supportedCountries;

  @override
  _ResidenceStepState createState() => _ResidenceStepState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IterableProperty<CountryModel>(
        'supportedCountries', supportedCountries));
  }
}

class _ResidenceStepState extends State<ResidenceStep> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  void _complete({CountryModel? model}) {
    if (model != null) {
      context.flow<RegistrationUserModel>().update(
          (RegistrationUserModel userModel) =>
              userModel.copyWith(residence: model));
    } else if (_controller.text.isNotEmpty) {
      final CountryModel newModel = CountryModel.fromString(_controller.text);
      context.flow<RegistrationUserModel>().update(
          (RegistrationUserModel userModel) =>
              userModel.copyWith(residence: newModel));
    }
  }

  @override
  Widget build(BuildContext context) {
    final InputBorder border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(14),
      borderSide: const BorderSide(width: 2),
    );
    return TemplateRegistrationStep(
      title: Text(AppLocalizations.of(context)?.yourContryOfResidence ??
          'yourContryOfResidence'),
      onNextPressed: _complete,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  AppLocalizations.of(context)?.yourContryOfResidence ??
                      'yourContryOfResidence',
                  style: Theme.of(context).textTheme.headline1,
                ),
                const SizedBox(height: 36),
                CountriesSelector(
                  countries: widget.supportedCountries,
                  onSelect: (CountryModel country) {},
                ),
                const SizedBox(height: 26),
                TextField(
                  controller: _controller,
                  textAlign: TextAlign.center,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                    focusedBorder: border,
                    enabledBorder: border,
                    hintText: '',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

///
typedef CountrySelectCallback = void Function(CountryModel country);

///
class CountriesSelector extends StatelessWidget {
  ///
  const CountriesSelector({
    required this.countries,
    required this.onSelect,
    Key? key,
  }) : super(key: key);

  ///
  final List<CountryModel> countries;

  ///
  final CountrySelectCallback onSelect;

  @override
  Widget build(BuildContext context) {
    final Locale locale = AppLocalization.of(context)?.currentLocale ??
        AppLocalization.defaultLocale;
    return ListView.separated(
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        final CountryModel model = countries[index];
        final String emoji = model.country.emoji;
        return AlesiaOutlineButton(
          color: AlesiaColors.lightBlue,
          onPressed: () => onSelect(model),
          child: Row(
            children: <Widget>[
              SizedBox(width: MediaQuery.of(context).size.width / 10),
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TwemojiTextSpan(
                      text: model.country.emoji,
                      twemojiFormat: TwemojiFormat.svg,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    TextSpan(
                      text: ' ${model.nameForLocale(locale)}',
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
              const Spacer(),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) =>
          const SizedBox(height: 16),
      itemCount: countries.length,
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(IterableProperty<CountryModel>('countries', countries))
      ..add(
          ObjectFlagProperty<CountrySelectCallback>.has('onSelect', onSelect));
  }
}
