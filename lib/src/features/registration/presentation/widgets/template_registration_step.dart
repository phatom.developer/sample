import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:alecia_app/src/widgets/buttons/fill_button.dart';
import 'package:alecia_app/src/widgets/buttons/text_back_button.dart';

/// Template of registration steps
class TemplateRegistrationStep extends StatelessWidget {
  /// Default construcotr
  const TemplateRegistrationStep({
    required this.title,
    required this.child,
    this.onBackPressed,
    Key? key,
    this.onNextPressed,
  }) : super(key: key);

  /// On back pressed, must clear previous property of current step
  final VoidCallback? onBackPressed;

  /// Title
  final Widget title;

  /// Void call back on next pressed. Should send the user to the next step
  final VoidCallback? onNextPressed;

  ///
  final Widget child;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          leading: TextBackButton(
            onPressed: onBackPressed,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: title,
          centerTitle: true,
          leadingWidth: 80,
          titleTextStyle: const TextStyle(
            color: Colors.black,
            fontSize: 17,
            fontWeight: FontWeight.w500,
          ),
          backwardsCompatibility: false,
        ),
        body: Column(
          children: <Widget>[
            Expanded(child: child),
            if (onNextPressed != null)
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 20,
                    ),
                  ],
                ),
                padding: const EdgeInsets.only(top: 10, bottom: 50),
                child: FillButton(
                  onPressed: onNextPressed,
                  child: Text(AppLocalizations.of(context)?.next ?? ''),
                ),
              ),
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
          ObjectFlagProperty<VoidCallback>.has('onBackPressed', onBackPressed))
      ..add(ObjectFlagProperty<VoidCallback?>.has(
          'onNextPressed', onNextPressed));
  }
}
