import 'package:flutter/material.dart';

/// Main app
class MainApp extends StatefulWidget {
  /// Default constructor
  const MainApp({Key? key}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) => Container(
        color: Colors.red,
        child: const Center(
          child: Text('Main application'),
        ),
      );
}
