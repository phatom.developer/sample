import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

/// Main splash screen of the app, using only on start app
class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        color: Colors.white,
        child: Center(
          child: SvgPicture.asset(
            'assets/svg/splash-star.svg',
            width: 130,
            height: 130,
          ),
        ),
      );
}
