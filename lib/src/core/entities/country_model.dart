import 'package:alecia_app/src/core/entities/country.dart';
import 'package:alecia_app/src/core/top_level_funcs/enum.dart';
import 'package:flutter/widgets.dart';

/// Country model
class CountryModel {
  ///
  const CountryModel(
    this.country, [
    this.name,
  ]);

  ///
  factory CountryModel.fromString(String value) {
    final Country? country = enumFromString(Country.values, value);
    if (country != null) {
      return CountryModel(country);
    } else {
      return CountryModel(Country.unknown, value);
    }
  }

  ///
  final Country country;

  ///
  final String? name;

  ///
  String nameForLocale(Locale locale) => name ?? country.rawValue;
}
