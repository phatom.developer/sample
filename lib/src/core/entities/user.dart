import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

import 'package:alecia_app/src/core/top_level_funcs/enum.dart';
import 'package:alecia_app/src/core/entities/characteristics/characteristic.dart';

// ignore: public_member_api_docs
typedef Json = Map<String, dynamic>;

@immutable
class User { 

final String name;
final String phone;
final String email;
final Gender gender;
final EyeColor eyeColor;
final HairColor hairColor;
final List<String> photoUrls;
final DocumentSnapshot<Json> snapshot;
final DocumentReference<Json> reference;
final String documentID;


User({
  required this.name,
  required this.phone,
  required this.email,
  required this.gender,
  required this.eyeColor,
  required this.hairColor,
  required this.photoUrls,
  required this.snapshot,
  required this.reference,
  required this.documentID,
});

static User? fromFirestore(DocumentSnapshot<Json> snapshot) {
  if (snapshot.data() == null) {
    return null;
  }
  final Json map = snapshot.data()!;
  final Gender gender = map['gender'] != null 
    ? enumFromString(Gender.values, map['gender']) ?? Gender.unknown
    : Gender.unknown;
  final EyeColor eyeColor = map['eyeColor'] != null 
    ? enumFromString(EyeColor.values, map['eyeColor']) ?? EyeColor.unknown
    : EyeColor.unknown;
  final HairColor hairColor = map['hairColor'] != null 
    ? enumFromString(HairColor.values, map['hairColor']) ?? HairColor.unknown
    : HairColor.unknown;
  return User(
    name: map['name'] != null ? map['name']! as String : '',
    phone: map['phone'] != null ? map['phone']! as String : '',
    email: map['email'] != null ? map['email']! as String : '',
    gender: gender,
    eyeColor: eyeColor,
    hairColor: hairColor,
    photoUrls: map['photoUrls'] != null 
    ? List<String>.from(map['photoUrls']) 
    : <String>[],
    snapshot: snapshot,
    reference: snapshot.reference,
    documentID: snapshot.id,
  ); 
}

Map<String, dynamic> toMap() => {
'name': name,
'phone': phone,
'email': email,
'gender': gender,
'eyeColor': eyeColor,
'hairColor': hairColor,
'photoUrls': photoUrls,
};

/// Copy with some new properties
User copyWith({
  String? name, 
  String? phone, 
  String? email, 
  Gender? gender, 
  EyeColor? eyeColor,
  HairColor? hairColor,
  List<String>? photoUrls,
}) => User(
name: name ?? this.name,
phone: phone ?? this.phone,
email: email ?? this.email,
gender: gender ?? this.gender,
eyeColor: eyeColor ?? this.eyeColor,
hairColor: hairColor ?? this.hairColor,
photoUrls: photoUrls ?? this.photoUrls,
snapshot: snapshot,
reference: reference,
documentID: documentID,
);

@override
String toString() {
// ignore: lines_longer_than_80_chars
return 'User${name.toString()}, ${phone.toString()}, ${email.toString()}, ${gender.toString()}, ${eyeColor.toString()}, ${hairColor.toString()}, ${photoUrls.toString()}, ';
}

@override
bool operator ==(other) => other is User && documentID == other.documentID;

int get hashCode => documentID.hashCode;}