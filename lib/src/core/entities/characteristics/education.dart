/// Education levels
enum Education {
  ///
  secondary,

  ///
  collegeDrgee,

  ///
  higherEducation,

  ///
  master,
}

/// Core extension
extension CoreEducation on Education {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
