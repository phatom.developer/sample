/// Hair coor of the user.
enum HairColor {
  /// Bald
  bald,

  /// Black
  black,

  /// Blond
  blond,

  /// Redhead
  redhead,

  /// Brown
  brown,

  /// Brune
  brune,

  /// Unknown
  unknown,
}

/// Core extension
extension CoreHairColor on HairColor {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
