import 'package:alecia_app/src/core/entities/language.dart';

/// Gender of the user.
enum Gender {
  /// Man
  man,

  /// Woman
  woman,

  /// Unknown
  unknown,
}

/// Core extension
extension CoreGender on Gender {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}

/// Getters [List<Language>] that UI supported
extension PropertiesGender on Gender {
  /// Supported UI language for current [Gender]
  List<Language> get interfaceLanguages {
    switch (this) {
      case Gender.man:
        return <Language>[
          Language.french,
          Language.english,
        ];
      case Gender.woman:
        return <Language>[
          Language.russian,
        ];
      case Gender.unknown:
        return <Language>[];
    }
  }
}
