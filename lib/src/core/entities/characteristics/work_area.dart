
/// Work area of the user
class WorkArea {

  /// Default constructor
  WorkArea({
    required this.type,
    this.description,
  }) : assert(
            type != WorkAreaType.other ||
                (description != null && description.isNotEmpty),
            'If type is WorkAreaType.other, description cant be null');
            
  /// Type of work area
  final WorkAreaType type;

  /// Description of the area, if [type] is equal [WorkAreaType.other],
  ///  must not be null.
  final String? description;
}

/// Type of work area
enum WorkAreaType {
  /// Secretary, administrator
  secretary,

  /// Accountant, economist
  economist,

  /// Physician, nurse
  nurse,

  /// Engineer, technical specialist
  engineer,

  /// Manager
  manager,

  /// Educator
  education,

  /// Own Business
  business,

  /// IT, Internet, Web
  it,

  /// Designer
  designer,

  /// Fitness or yoga instructor
  fitness,

  /// Pschylologist
  psychologist,

  /// Other
  other,
}

/// Core extension
extension CoreWorkAreaType on WorkAreaType {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}