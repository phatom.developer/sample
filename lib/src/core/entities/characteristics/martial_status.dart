/// Martial status of the user
enum MartialStatus {
  ///
  single,

  ///
  divorced,

  ///
  widowed,
}

/// Core extension
extension CoreMartialStatus on MartialStatus {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
