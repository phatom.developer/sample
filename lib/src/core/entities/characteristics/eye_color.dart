/// Eye color of the user.
enum EyeColor {
  /// Black
  black,

  /// Blue
  blue,

  /// Brown
  brown,

  /// Green
  green,

  /// Grey
  grey,

  /// Grey blue
  greyBlue,

  /// Hazel
  hazel,

  /// Unknown
  unknown,
}

/// Core extension
extension CoreEyeColor on EyeColor {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
