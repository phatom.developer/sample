/// Types of sports that the user is engaged
enum Sport {
  ///
  tennis,

  ///
  ball,

  ///
  other,
}

/// Core extension
extension CoreSport on Sport {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
