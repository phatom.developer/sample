import 'package:alecia_app/src/core/entities/language.dart';
import 'package:alecia_app/src/core/entities/characteristics/gender.dart';

/// Language skills of the user
class LanguageSkills {
  /// Default constructor
  LanguageSkills({
    required List<LanguageSkill> languageSkills,
    required this.other,
    required Gender gender,
  }) : languageSkills = _configureSkills(languageSkills, gender);

  /// All preffered skills for user for cuncreate gender
  final List<LanguageSkill> languageSkills;

  /// Other languages must be written user
  final String? other;

  /// Preffered languages, that the user skill must have for specify gender
  static List<Language> prefferedLanguages(Gender gender) {
    switch (gender) {
      case Gender.man:
        return <Language>[
          Language.russian,
          Language.french,
          Language.german,
          Language.italian,
          Language.spanish,
        ];
      case Gender.woman:
        return <Language>[
          Language.english,
          Language.french,
          Language.german,
          Language.italian,
          Language.spanish,
        ];
      case Gender.unknown:
        return <Language>[];
    }
  }

  static List<LanguageSkill> _configureSkills(
    List<LanguageSkill> skills,
    Gender gender,
  ) =>
      prefferedLanguages(gender)
          .map<LanguageSkill>((Language language) => skills.firstWhere(
              (LanguageSkill skill) => skill.language == language,
              orElse: () =>
                  LanguageSkill(language: language, level: SkillLevel.none)))
          .toList();
}

/// Language skill
class LanguageSkill {
  /// Default constrictor
  const LanguageSkill({
    required this.language,
    required this.level,
  });

  /// Language
  final Language language;

  /// Level
  final SkillLevel level;

  /// Create language skill with new skill level
  LanguageSkill updateLevel(SkillLevel level) => LanguageSkill(
        language: language,
        level: level,
      );
}

/// Language skill level
enum SkillLevel {
  /// None level
  none,

  /// Low level
  low,

  /// Medium level
  medium,

  /// Heigh level
  heigh,
}

/// Core extension
extension CoreSkillLevel on SkillLevel {
  /// Only name  of the enum
  String get rawValue => toString().split('.').last;

  /// Language level int value
  int get level => SkillLevel.values.indexOf(this);
}
