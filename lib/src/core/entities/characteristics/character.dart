/// Character of the user
enum Character {
  ///
  generosity,

  ///
  devotion,

  ///
  patience,

  ///
  charisma,

  ///
  shy,
}

/// Core extension
extension CoreCharacter on Character {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}

/// Supported properties
extension PropertiesCharacter on Character {
  /// Every character must have unique emoji
  String get emoji {
    // TODO: add all emoji
    return '';
  }
}
