/// Supported structure for organizachildren info of the user
class Children {
  /// Default constricor
  Children({
    required this.boys,
    required this.girls,
  });

  ///
  final int boys;

  ///
  final int girls;

  /// Summary count of children
  int get count => boys + girls;

  /// Boys emoji
  String get boyEmoji => '👦';

  /// Girls emoji
  String get girlEmoji => '👧';
}
