/// Hobby of the user
enum Hobby {
  ///
  generosity,

  ///
  patience,

  ///
  charisma,
}

/// Core extension
extension CoreHobbi on Hobby {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}
