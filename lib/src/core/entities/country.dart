import 'package:alecia_app/src/core/entities/characteristics/gender.dart';

enum Country {
  afghanistan,
  alandIslands,
  albania,
  algeria,
  americanSamoa,
  andorra,
  angola,
  anguilla,
  antiguaAndBarbuda,
  argentina,
  armenia,
  aruba,
  australia,
  austria,
  azerbaijan,
  bahamas,
  bahrain,
  bangladesh,
  barbados,
  belarus,
  belgium,
  belize,
  benin,
  bermuda,
  bhutan,
  bolivia,
  bosniaAndHerzegovina,
  botswana,
  brazil,
  britishVirginIslands,
  brunei,
  bulgaria,
  burkinaFaso,
  burundi,
  cambodia,
  cameroon,
  canada,
  capeVerde,
  caymanIslands,
  centralAfricanRepublic,
  chad,
  chile,
  china,
  colombia,
  comoros,
  costaRica,
  croatia,
  cyprus,
  czechRepublic,
  democraticRepublicOfTheCongo,
  denmark,
  djibouti,
  dominica,
  dominicanRepublic,
  ecuador,
  egypt,
  elSalvador,
  equatorialGuinea,
  eritrea,
  estonia,
  ethiopia,
  falklandIslands,
  faroeIslands,
  federatedStatesOfMicronesia,
  fiji,
  finland,
  france,
  frenchGuiana,
  frenchPolynesia,
  gabon,
  gambia,
  georgia,
  germany,
  ghana,
  gibraltar,
  greece,
  greenland,
  grenada,
  guadeloupe,
  guam,
  guatemala,
  guernsey,
  guinea,
  guyana,
  haiti,
  honduras,
  hongKong,
  hungary,
  iceland,
  india,
  indonesia,
  iraq,
  ireland,
  isleOfMan,
  israel,
  italy,
  jamaica,
  japan,
  jersey,
  jordan,
  kazakhstan,
  kenya,
  kiribati,
  kuwait,
  kyrgyzstan,
  laos,
  latvia,
  lebanon,
  lesotho,
  libya,
  liechtenstein,
  lithuania,
  luxembourg,
  macau,
  macedonia,
  madagascar,
  malawi,
  malaysia,
  maldives,
  mali,
  malta,
  marshallIslands,
  martinique,
  mauritania,
  mauritius,
  mayotte,
  mexico,
  moldova,
  monaco,
  mongolia,
  montenegro,
  morocco,
  mozambique,
  namibia,
  nauru,
  nepal,
  netherlands,
  netherlandsAntilles,
  newCaledonia,
  newZealand,
  nicaragua,
  niger,
  nigeria,
  northernMarianaIslands,
  norway,
  oman,
  pakistan,
  palau,
  palestinianAuthority,
  panama,
  papuaNewGuinea,
  paraguay,
  peru,
  philippines,
  poland,
  portugal,
  puertoRico,
  qatar,
  republicOfTheCongo,
  romania,
  russia,
  rwanda,
  reunion,
  saintKittsAndNevis,
  saintVincentAndTheGrenadines,
  samoa,
  sanMarino,
  saoTomeAndPrincipe,
  saudiArabia,
  senegal,
  serbia,
  seychelles,
  sierraLeone,
  singapore,
  slovakia,
  slovenia,
  solomonIslands,
  somalia,
  southAfrica,
  southKorea,
  spain,
  sriLanka,
  stLucia,
  suriname,
  swaziland,
  sweden,
  switzerland,
  taiwan,
  tajikistan,
  tanzania,
  thailand,
  togo,
  tonga,
  trinidadAndTobago,
  tunisia,
  turkey,
  turkmenistan,
  turksAndCaicosIslands,
  tuvalu,
  uganda,
  ukraine,
  unitedArabEmirates,
  unitedKingdom,
  unitedStates,
  uruguay,
  usVirginIslands,
  uzbekistan,
  vanuatu,
  vaticanCity,
  venezuela,
  vietnam,
  yemen,
  zambia,
  unknown,
}

/// Emojies for country
extension Emoji on Country {
  /// Default emoji
  String get emoji {
    switch (this) {
      case Country.afghanistan:
        return '🇦🇫';
      case Country.alandIslands:
        return '🇦🇽';
      case Country.albania:
        return '🇦🇱';
      case Country.algeria:
        return '🇩🇿';
      case Country.americanSamoa:
        return '🇦🇸';
      case Country.andorra:
        return '🇦🇩';
      case Country.angola:
        return '🇦🇴';
      case Country.anguilla:
        return '🇦🇮';
      case Country.antiguaAndBarbuda:
        return '🇦🇬';
      case Country.argentina:
        return '🇦🇷';
      case Country.armenia:
        return '🇦🇲';
      case Country.aruba:
        return '🇦🇼';
      case Country.australia:
        return '🇦🇺';
      case Country.austria:
        return '🇦🇹';
      case Country.azerbaijan:
        return '🇦🇿';
      case Country.bahamas:
        return '🇧🇸';
      case Country.bahrain:
        return '🇧🇭';
      case Country.bangladesh:
        return '🇧🇩';
      case Country.barbados:
        return '🇧🇧';
      case Country.belarus:
        return '🇧🇾';
      case Country.belgium:
        return '🇧🇪';
      case Country.belize:
        return '🇧🇿';
      case Country.benin:
        return '🇧🇯';
      case Country.bermuda:
        return '🇧🇲';
      case Country.bhutan:
        return '🇧🇹';
      case Country.bolivia:
        return '🇧🇴';
      case Country.bosniaAndHerzegovina:
        return '🇧🇦';
      case Country.botswana:
        return '🇧🇼'; // todo
      case Country.brazil:
        return '🇧🇷';
      case Country.britishVirginIslands: // todo
        // TODO: Handle this case.
        break;
      case Country.brunei:
        return '🇧🇳';
      case Country.bulgaria:
        return '🇧🇬';
      case Country.burkinaFaso:
        return '🇧🇫';
      case Country.burundi:
        return '🇧🇮';
      case Country.cambodia:
        return '🇰🇭';
      case Country.cameroon:
        return '🇨🇲';
      case Country.canada:
        return '🇨🇦';
      case Country.capeVerde:
        return '🇨🇻';
      case Country.caymanIslands: // todo miss
        return '🇰🇾';
      case Country.centralAfricanRepublic:
        return '🇨🇫';
      case Country.chad:
        return '🇹🇩';
      case Country.chile:
        return '🇨🇱';
      case Country.china:
        return '🇨🇳';
      case Country.colombia: // todo miss 2
        return '🇨🇴';
      case Country.comoros:
        return '🇰🇲';
      case Country.costaRica: // todo miss 3
        return '🇨🇷';
      case Country.croatia: // todo miss 1
        return '🇭🇷';
      case Country.cyprus: // todo miss 2
        return '🇨🇾';
      case Country.czechRepublic: // todo miss
        // TODO: Handle this case.
        break;
      case Country.democraticRepublicOfTheCongo: // todo
        // TODO: Handle this case.
        break;
      case Country.denmark:
        return '🇩🇰';
      case Country.djibouti:
        return '🇩🇯';
      case Country.dominica:
        return '🇩🇲';
      case Country.dominicanRepublic:
        return '🇩🇴';
      case Country.ecuador:
        return '🇪🇨';
      case Country.egypt:
        return '🇪🇬';
      case Country.elSalvador:
        return '🇸🇻';
      case Country.equatorialGuinea: // todo miss
        return '🇬🇶';
      case Country.eritrea:
        return '🇪🇷';
      case Country.estonia:
        return '🇪🇪';
      case Country.ethiopia: // todo miss 1
        return '🇪🇹';
      case Country.falklandIslands:
        return '🇫🇰';
      case Country.faroeIslands:
        return '🇫🇴';
      case Country.federatedStatesOfMicronesia: // todo
        // TODO: Handle this case.
        break;
      case Country.fiji:
        return '🇫🇯';
      case Country.finland:
        return '🇫🇮';
      case Country.france:
        return '🇫🇷';
      case Country.frenchGuiana:
        return '🇬🇫';
      case Country.frenchPolynesia:
        return '🇵🇫';
      case Country.gabon: // todo miss
        return '🇬🇦';
      case Country.gambia:
        return '🇬🇲';
      case Country.georgia:
        return '🇬🇪';
      case Country.germany:
        return '🇩🇪';
      case Country.ghana:
        return '🇬🇭';
      case Country.gibraltar:
        return '🇬🇮';
      case Country.greece:
        return '🇬🇷';
      case Country.greenland:
        return '🇬🇱';
      case Country.grenada:
        return '🇬🇩';
      case Country.guadeloupe:
        return '🇬🇵';
      case Country.guam:
        return '🇬🇺';
      case Country.guatemala:
        return '🇬🇹';
      case Country.guernsey:
        return '🇬🇬';
      case Country.guinea:
        return '🇬🇳';
      case Country.guyana: // todo miss
        return '🇬🇾';
      case Country.haiti:
        return '🇭🇹'; // next
      case Country.honduras:
        // TODO: Handle this case.
        break;
      case Country.hongKong:
        // TODO: Handle this case.
        break;
      case Country.hungary:
        // TODO: Handle this case.
        break;
      case Country.iceland:
        // TODO: Handle this case.
        break;
      case Country.india:
        // TODO: Handle this case.
        break;
      case Country.indonesia:
        // TODO: Handle this case.
        break;
      case Country.iraq:
        // TODO: Handle this case.
        break;
      case Country.ireland:
        // TODO: Handle this case.
        break;
      case Country.isleOfMan:
        // TODO: Handle this case.
        break;
      case Country.israel:
        // TODO: Handle this case.
        break;
      case Country.italy:
        // TODO: Handle this case.
        break;
      case Country.jamaica:
        // TODO: Handle this case.
        break;
      case Country.japan:
        // TODO: Handle this case.
        break;
      case Country.jersey:
        // TODO: Handle this case.
        break;
      case Country.jordan:
        // TODO: Handle this case.
        break;
      case Country.kazakhstan:
        // TODO: Handle this case.
        break;
      case Country.kenya:
        // TODO: Handle this case.
        break;
      case Country.kiribati:
        // TODO: Handle this case.
        break;
      case Country.kuwait:
        // TODO: Handle this case.
        break;
      case Country.kyrgyzstan:
        // TODO: Handle this case.
        break;
      case Country.laos:
        // TODO: Handle this case.
        break;
      case Country.latvia:
        // TODO: Handle this case.
        break;
      case Country.lebanon:
        // TODO: Handle this case.
        break;
      case Country.lesotho:
        // TODO: Handle this case.
        break;
      case Country.libya:
        // TODO: Handle this case.
        break;
      case Country.liechtenstein:
        // TODO: Handle this case.
        break;
      case Country.lithuania:
        // TODO: Handle this case.
        break;
      case Country.luxembourg:
        // TODO: Handle this case.
        break;
      case Country.macau:
        // TODO: Handle this case.
        break;
      case Country.macedonia:
        // TODO: Handle this case.
        break;
      case Country.madagascar:
        // TODO: Handle this case.
        break;
      case Country.malawi:
        // TODO: Handle this case.
        break;
      case Country.malaysia:
        // TODO: Handle this case.
        break;
      case Country.maldives:
        // TODO: Handle this case.
        break;
      case Country.mali:
        // TODO: Handle this case.
        break;
      case Country.malta:
        // TODO: Handle this case.
        break;
      case Country.marshallIslands:
        // TODO: Handle this case.
        break;
      case Country.martinique:
        // TODO: Handle this case.
        break;
      case Country.mauritania:
        // TODO: Handle this case.
        break;
      case Country.mauritius:
        // TODO: Handle this case.
        break;
      case Country.mayotte:
        // TODO: Handle this case.
        break;
      case Country.mexico:
        // TODO: Handle this case.
        break;
      case Country.moldova:
        // TODO: Handle this case.
        break;
      case Country.monaco:
        // TODO: Handle this case.
        break;
      case Country.mongolia:
        // TODO: Handle this case.
        break;
      case Country.montenegro:
        // TODO: Handle this case.
        break;
      case Country.morocco:
        // TODO: Handle this case.
        break;
      case Country.mozambique:
        // TODO: Handle this case.
        break;
      case Country.namibia:
        // TODO: Handle this case.
        break;
      case Country.nauru:
        // TODO: Handle this case.
        break;
      case Country.nepal:
        // TODO: Handle this case.
        break;
      case Country.netherlands:
        // TODO: Handle this case.
        break;
      case Country.netherlandsAntilles:
        // TODO: Handle this case.
        break;
      case Country.newCaledonia:
        // TODO: Handle this case.
        break;
      case Country.newZealand:
        // TODO: Handle this case.
        break;
      case Country.nicaragua:
        // TODO: Handle this case.
        break;
      case Country.niger:
        // TODO: Handle this case.
        break;
      case Country.nigeria:
        // TODO: Handle this case.
        break;
      case Country.northernMarianaIslands:
        // TODO: Handle this case.
        break;
      case Country.norway:
        // TODO: Handle this case.
        break;
      case Country.oman:
        // TODO: Handle this case.
        break;
      case Country.pakistan:
        // TODO: Handle this case.
        break;
      case Country.palau:
        // TODO: Handle this case.
        break;
      case Country.palestinianAuthority:
        // TODO: Handle this case.
        break;
      case Country.panama:
        // TODO: Handle this case.
        break;
      case Country.papuaNewGuinea:
        // TODO: Handle this case.
        break;
      case Country.paraguay:
        // TODO: Handle this case.
        break;
      case Country.peru:
        // TODO: Handle this case.
        break;
      case Country.philippines:
        // TODO: Handle this case.
        break;
      case Country.poland:
        // TODO: Handle this case.
        break;
      case Country.portugal:
        // TODO: Handle this case.
        break;
      case Country.puertoRico:
        // TODO: Handle this case.
        break;
      case Country.qatar:
        // TODO: Handle this case.
        break;
      case Country.republicOfTheCongo:
        // TODO: Handle this case.
        break;
      case Country.romania:
        // TODO: Handle this case.
        break;
      case Country.russia:
        // TODO: Handle this case.
        break;
      case Country.rwanda:
        // TODO: Handle this case.
        break;
      case Country.reunion:
        // TODO: Handle this case.
        break;
      case Country.saintKittsAndNevis:
        // TODO: Handle this case.
        break;
      case Country.saintVincentAndTheGrenadines:
        // TODO: Handle this case.
        break;
      case Country.samoa:
        // TODO: Handle this case.
        break;
      case Country.sanMarino:
        // TODO: Handle this case.
        break;
      case Country.saoTomeAndPrincipe:
        // TODO: Handle this case.
        break;
      case Country.saudiArabia:
        // TODO: Handle this case.
        break;
      case Country.senegal:
        // TODO: Handle this case.
        break;
      case Country.serbia:
        // TODO: Handle this case.
        break;
      case Country.seychelles:
        // TODO: Handle this case.
        break;
      case Country.sierraLeone:
        // TODO: Handle this case.
        break;
      case Country.singapore:
        // TODO: Handle this case.
        break;
      case Country.slovakia:
        // TODO: Handle this case.
        break;
      case Country.slovenia:
        // TODO: Handle this case.
        break;
      case Country.solomonIslands:
        // TODO: Handle this case.
        break;
      case Country.somalia:
        // TODO: Handle this case.
        break;
      case Country.southAfrica:
        // TODO: Handle this case.
        break;
      case Country.southKorea:
        // TODO: Handle this case.
        break;
      case Country.spain:
        // TODO: Handle this case.
        break;
      case Country.sriLanka:
        // TODO: Handle this case.
        break;
      case Country.stLucia:
        // TODO: Handle this case.
        break;
      case Country.suriname:
        // TODO: Handle this case.
        break;
      case Country.swaziland:
        // TODO: Handle this case.
        break;
      case Country.sweden:
        // TODO: Handle this case.
        break;
      case Country.switzerland:
        // TODO: Handle this case.
        break;
      case Country.taiwan:
        // TODO: Handle this case.
        break;
      case Country.tajikistan:
        // TODO: Handle this case.
        break;
      case Country.tanzania:
        // TODO: Handle this case.
        break;
      case Country.thailand:
        // TODO: Handle this case.
        break;
      case Country.togo:
        // TODO: Handle this case.
        break;
      case Country.tonga:
        // TODO: Handle this case.
        break;
      case Country.trinidadAndTobago:
        // TODO: Handle this case.
        break;
      case Country.tunisia:
        // TODO: Handle this case.
        break;
      case Country.turkey:
        // TODO: Handle this case.
        break;
      case Country.turkmenistan:
        // TODO: Handle this case.
        break;
      case Country.turksAndCaicosIslands:
        // TODO: Handle this case.
        break;
      case Country.tuvalu:
        // TODO: Handle this case.
        break;
      case Country.uganda:
        // TODO: Handle this case.
        break;
      case Country.ukraine:
        // TODO: Handle this case.
        break;
      case Country.unitedArabEmirates:
        // TODO: Handle this case.
        break;
      case Country.unitedKingdom:
        // TODO: Handle this case.
        break;
      case Country.unitedStates:
        // TODO: Handle this case.
        break;
      case Country.uruguay:
        // TODO: Handle this case.
        break;
      case Country.usVirginIslands:
        // TODO: Handle this case.
        break;
      case Country.uzbekistan:
        // TODO: Handle this case.
        break;
      case Country.vanuatu:
        // TODO: Handle this case.
        break;
      case Country.vaticanCity:
        // TODO: Handle this case.
        break;
      case Country.venezuela:
        // TODO: Handle this case.
        break;
      case Country.vietnam:
        // TODO: Handle this case.
        break;
      case Country.yemen:
        // TODO: Handle this case.
        break;
      case Country.zambia:
        // TODO: Handle this case.
        break;
      case Country.unknown:
        // TODO: Handle this case.
        break;
    }
    return '🏴';
  }
}

/// Core extension
extension CoreCountry on Country {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;
}

///
abstract class Countries {
  ///
  static List<Country> residenceFor(Gender gender) {
    switch (gender) {
      case Gender.man:
        return <Country>[
          Country.france,
          Country.unitedKingdom,
          Country.belgium,
        ];
      case Gender.woman:
        return <Country>[
          Country.belarus,
          Country.russia,
          Country.ukraine,
        ];
      case Gender.unknown:
        return <Country>[];
    }
  }
}
