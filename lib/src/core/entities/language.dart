/// Languages, that be used for some reason in the app
enum Language {
  ///
  english,

  ///
  french,

  ///
  russian,

  ///
  german,

  ///
  italian,

  ///
  spanish,
}

/// Core extension
extension CoreLanguage on Language {
  /// Raw value of the enum
  String get rawValue => toString().split('.').last;

  /// If this language supported for UI, return his locale code, else 
  /// return default "en"
  String get languageCode {
    switch (this) {
      case Language.french:
        return 'fr';
      case Language.russian:
        return 'ru';
      case Language.german:
        return 'de';
      case Language.english:
      case Language.italian:
      case Language.spanish:
        return 'en';
    }
  }
}
