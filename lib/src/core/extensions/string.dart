/// Core extensions for maybe null [String]
extension MaybeNullCoreString on String? {
  /// Shortcat for unwrap nil string to emty string
  String get orEmpty => this ?? '';

  ///
}

/// Core extensions for [String]
extension CoreString on String {
  /// Capitalize [String]. Make first letter upperCase.
  /// For example 'some string' changes 'Some string'
  String get capitalize => '${this[0].toUpperCase()}${substring(1)}';
}
