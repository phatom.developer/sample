/// Supported constants, convertation from europen SI
abstract class _MeasureUnits {
  /// Foot
  static const num foot = 30.48;
}

/// Unit conversions
extension MeasureUnits on num {
  /// Centimeters to foots
  num get fromCmToFoot => this / _MeasureUnits.foot;

  /// Foots to centimeters
  num get fromFootToCm => this * _MeasureUnits.foot;
}
