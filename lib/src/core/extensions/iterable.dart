///
// ignore: always_specify_types
extension CoreIterable<E> on Iterable<E> {
  ///
  // ignore: avoid_annotating_with_dynamic
  Iterable<E> compact() 
  // ignore: always_specify_types
  => where((E element) => element != null);
}
