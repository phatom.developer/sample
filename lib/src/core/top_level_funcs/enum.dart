/// We save every enum without his type.
/// Every enum must have [rawValue] extension, for take his string value
/// ```dart
/// enum MyEnum {
///   first,
///   second,
/// }
///
/// extension on MyEnum {
///   String get rawValue {
///     return toString().split('.').last;
///   }
/// }
/// ```
T? enumFromString<T>(Iterable<T> values, String? value) {
  try {
    return values.firstWhere((T e) => e.toString().split('.').last == value);
    // ignore: avoid_catching_errors
  } on StateError {
    return null;
  }
}
