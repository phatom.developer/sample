import 'package:flutter/material.dart';

// ignore: avoid_classes_with_only_static_members
/// Global support cla≥ss for locale
class L10n {
  /// All supported locales in this app
  static const List<Locale> supportedLocales = <Locale>[
    Locale.fromSubtags(languageCode: 'en'),
    Locale.fromSubtags(languageCode: 'ru'),
    Locale.fromSubtags(languageCode: 'fr'),
  ];

  /// Default locale of the app
  static Locale get defaultLocale => supportedLocales.first;
}
