import 'package:alecia_app/src/widgets/unfocus_detector.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:alecia_app/src/features/auth/presentation/screens/auth_guard.dart';
import 'package:alecia_app/src/features/localization/presentation/localization_scope.dart';
import 'package:alecia_app/src/features/main_app/presentation/screens/main_app.dart';
import 'package:alecia_app/src/features/registration/presentation/screens/registration_flow.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(AlesiaApp());
}

/// App widget
class AlesiaApp extends StatefulWidget {
  @override
  _AlesiaAppState createState() => _AlesiaAppState();
}

class _AlesiaAppState extends State<AlesiaApp> {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Alecia',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            textTheme: const TextTheme(
              headline1: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.black,
                fontSize: 22,
              ),
            )),
        home: const UnfocusDetector(
          child: AppLocalization(
            child: AuthGuard(
              authScreen: RegistrationUserFlow(),
              child: MainApp(),
            ),
          ),
        ),
      );
}
